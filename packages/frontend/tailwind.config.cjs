module.exports = {
    purge: [],
    darkMode: "class", // false, 'media', or 'class'
    theme: {
        extend: {
            body: ["Open Sans"]
        }
    },
    variants: {},
    plugins: []
};
