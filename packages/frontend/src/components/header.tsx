import { Link as Link, useLocation } from "wouter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserTie } from "@fortawesome/free-solid-svg-icons";
import React, { useEffect, useState } from "preact/compat";
import { storage } from "../lib/api";
import { StorageChangeListener } from "@flowtr/panel-sdk";
import { Flex } from "./ui/flex";
import Logo from "../images/logo.png";
import { Dropdown, DropdownItem } from "./ui/dropdown";

export const Header = () => {
    const [token, setToken] = useState<string | undefined>(undefined);

    const handleStorage: StorageChangeListener = (changes) => {
        const newVal = changes["token"].newValue;
        if (
            newVal !== undefined &&
            newVal !== null &&
            changes["token"].oldValue !== newVal
        )
            setToken(newVal);
        else setToken(undefined);
    };

    useEffect(() => {
        setToken(storage.get("token").token);

        storage.onChanged.addListener(handleStorage);

        return () => {
            if (storage.onChanged.hasListener(handleStorage))
                storage.onChanged.removeListener(handleStorage);
        };
    }, [token]);

    const [, setLocation] = useLocation();
    return (
        <Flex
            justify="center"
            align="center"
            className="h-auto p-6 w-full bg-gray-900"
        >
            <h1 className="text-pink-500 font-bold">
                <Link href="/">
                    <img
                        src={Logo}
                        className="w-36"
                        alt="Flowtr"
                        label="Flowtr Logo"
                    />
                </Link>
            </h1>
            <Flex
                className="header-navigation ml-auto mr-6"
                justify="center"
                align="center"
            >
                <Dropdown
                    buttonIcon={
                        <FontAwesomeIcon
                            className="mb-2 text-yellow-600"
                            icon={faUserTie}
                            size={"2x"}
                            alt="Links"
                        />
                    }
                >
                    {token && (
                        <DropdownItem
                            onClick={() =>
                                setLocation("/dashboard/deployments")
                            }
                        >
                            Deployments
                        </DropdownItem>
                    )}
                    <DropdownItem
                        onClick={() =>
                            setLocation(
                                token !== undefined
                                    ? "/dashboard/profile"
                                    : "/dashboard/login"
                            )
                        }
                    >
                        {token ? "Profile" : "Login"}
                    </DropdownItem>
                    {token && (
                        <DropdownItem
                            onClick={() => {
                                storage.remove("token");
                                setLocation("/dashboard/login");
                            }}
                        >
                            Log Out
                        </DropdownItem>
                    )}
                </Dropdown>
            </Flex>
        </Flex>
    );
};
