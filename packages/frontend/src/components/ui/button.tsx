import { RenderableProps } from "preact";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleNotch } from "@fortawesome/free-solid-svg-icons";

export const Button = (
    props: RenderableProps<{
        type?: "submit";
        isLoading?: boolean;
        className?: string;
        onClick?: () => void;
    }>
) => (
    <>
        {props.isLoading ? (
            <FontAwesomeIcon
                icon={faCircleNotch}
                size="4x"
                className="animate-spin"
            />
        ) : (
            <button
                className={`${props.className} bg-gray-900 text-white font-bold py-2 px-4 rounded`}
                type={props.type}
                onClick={props.onClick && props.onClick}
            >
                {props.children}
            </button>
        )}
    </>
);
