import { JSX, RenderableProps } from "preact";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/free-solid-svg-icons";

export const Flex = (
    props: RenderableProps<{
        direction?: "col" | "row";
        justify?: "center" | "start" | "end";
        align?: "center" | "start" | "end";
        flexWrap?: boolean;
        isLoading?: boolean;
        className?: string;
    }>
) => (
    <>
        {props.isLoading ? (
            <FontAwesomeIcon icon={faCircle} className="animate-spin" />
        ) : (
            <div
                {...props}
                className={`${props.className} flex flex-${
                    props.direction ?? "row"
                } justify-${props.justify} items-${props.align} ${
                    props.flexWrap ? "flex-wrap" : "flex-nowrap"
                }`}
            >
                {props.children}
            </div>
        )}
    </>
);
