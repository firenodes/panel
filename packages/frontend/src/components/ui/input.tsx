import { RenderableProps, JSX } from "preact";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/free-solid-svg-icons";

export const Input = (
    props: RenderableProps<
        {
            type?: "submit" | "text" | "email" | "password";
            isLoading?: boolean;
        } & JSX.HTMLAttributes<HTMLInputElement>
    >
) => (
    <>
        {props.isLoading ? (
            <FontAwesomeIcon icon={faCircle} className="animate-spin" />
        ) : (
            <input
                type={props.type ?? "text"}
                {...props}
                className={`${props.className} bg-gray-800 hover:bg-gray-900 hover:border-green-600 border-2 text-white font-bold py-3 px-4 rounded-lg`}
            >
                {props.children}
            </input>
        )}
    </>
);
