import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { RenderableProps, VNode } from "preact";
import { useState } from "preact/hooks";

export const Dropdown = (
    props: RenderableProps<{
        buttonText?: VNode;
        buttonIcon?: VNode;
    }>
) => {
    const [enabled, setEnabled] = useState(false);

    return (
        <>
            <button
                className="p-2 w-28 rounded-full overflow-hidden border-2 bg-gray-800 border-gray-600 focus:outline-none focus:border-white"
                onClick={() => setEnabled(!enabled)}
            >
                {props.buttonText}
                {props.buttonIcon || (
                    <FontAwesomeIcon
                        className="ml-2"
                        icon={faCaretDown}
                        transform={{ rotate: enabled ? 0 : -90 }}
                    />
                )}
            </button>
            <DropdownList enabled={enabled}>{props.children}</DropdownList>
        </>
    );
};

const DropdownList = (props: RenderableProps<{ enabled: boolean }>) =>
    props.enabled ? (
        <div className="absolute z-20 mt-36 py-2 w-48 text-white bg-gray-800 rounded-lg shadow-xl">
            {props.children}
        </div>
    ) : (
        <div />
    );

export const DropdownItem = (
    props: RenderableProps<{ onClick: () => void }>
) => (
    <button
        onClick={props.onClick}
        className="block px-4 py-2 w-full bg-gray-900 text-white hover:text-blue-400"
    >
        {props.children}
    </button>
);
