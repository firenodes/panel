import { Router, Route } from "wouter";
import { Header } from "./header";
import { Profile } from "./profile";
import { Login } from "./login";
import { ToastContainer } from "react-toastify";
import { DeploymentList } from "./deployment-list";
import { DeployForm } from "./deploy";
import { DeploymentInfo } from "./deployment-info";
import { Flex } from "./ui/flex";

export const Layout = () => (
    <Router>
        <Header />
        <Flex
            justify="start"
            align="center"
            direction="col"
            className="p-8 w-full min-h-screen bg-gray-800 text-white"
        >
            <ToastContainer
                position="top-right"
                autoClose={5000}
                pauseOnHover
            />
            <Route path="/">
                {/*TODO: configurable welcome message*/}
                <h2 className="font-extrabold">
                    Welcome to your new panel instance.
                </h2>
            </Route>
            <Route path="/dashboard/login">
                <Login />
            </Route>
            <Route path="/dashboard/profile">
                <Profile />
            </Route>
            <Route path="/dashboard/deployments">
                <DeploymentList />
            </Route>
            <Route path="/dashboard/deploy">
                <DeployForm />
            </Route>
            <Route path="/dashboard/deployment/:id">
                {(params: { id: string }) => (
                    <DeploymentInfo id={params.id}></DeploymentInfo>
                )}
            </Route>
        </Flex>
    </Router>
);
