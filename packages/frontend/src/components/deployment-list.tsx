import { IDeployment, toTitleCase } from "@flowtr/panel-sdk";
import { useEffect, useState } from "preact/hooks";
import { api } from "../lib/api";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faCloudUploadAlt,
    faCircle,
    faMinus,
    faRedo
} from "@fortawesome/free-solid-svg-icons";
import { useLocation } from "wouter";
import { Flex } from "./ui/flex";
import { Button } from "./ui/button";

export const DeploymentList = () => {
    const [containers, setContainers] = useState<IDeployment[]>([]);
    const [reload, setReload] = useState<boolean>(false);
    const [, setLocation] = useLocation();

    useEffect(() => {
        api.getDeployments().then((res) => setContainers(res.data.containers));
    }, [reload]);

    return (
        <Flex justify="center" align="center" direction="col" flexWrap>
            <h2>Deployment List</h2>
            <Flex justify="center" align="center" flexWrap>
                <Button
                    onClick={() => setLocation("/dashboard/deploy")}
                    className="mb-6 mr-6"
                >
                    <FontAwesomeIcon title="Deploy" icon={faCloudUploadAlt} />
                    <p className="ml-2">Deploy</p>
                </Button>
                <Button onClick={() => setReload(!reload)} className="mb-6">
                    <FontAwesomeIcon icon={faRedo} />
                    <p className="ml-2">Reload</p>
                </Button>
            </Flex>
            <Flex justify="center" align="center" direction="col" flexWrap>
                {containers.length === 0 && <p>No containers found.</p>}
                {containers.map((c) => (
                    <Button
                        key={c.id}
                        onClick={() =>
                            setLocation(`/dashboard/deployment/${c.id}`)
                        }
                        className="mb-6"
                    >
                        <Flex justify="center" align="center" className="w-1/3">
                            <p className="ml-2 mr-2">{toTitleCase(c.name)}</p>
                            <FontAwesomeIcon icon={faMinus} />
                            <p className="ml-2 mr-2">
                                {toTitleCase(c.image.split(":")[0])}
                            </p>
                            <FontAwesomeIcon icon={faMinus} />
                            <FontAwesomeIcon
                                className="ml-2 mr-2"
                                size={"2x"}
                                icon={faCircle}
                                bg={c.status ? "#42f563" : "#f56642"}
                            />
                        </Flex>
                    </Button>
                ))}
            </Flex>
        </Flex>
    );
};
