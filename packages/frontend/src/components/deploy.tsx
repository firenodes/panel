import { api } from "../lib/api";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faCloudUploadAlt,
    faPaperPlane
} from "@fortawesome/free-solid-svg-icons";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { useLocation } from "wouter";
import { Flex } from "./ui/flex";
import { Input } from "./ui/input";
import { Button } from "./ui/button";

export interface DeployFormData {
    name: string;
    image: string;
}

export const DeployForm = () => {
    const {
        handleSubmit,
        register,
        formState: { errors, isSubmitting }
    } = useForm<DeployFormData>();
    const [, setLocation] = useLocation();

    const onSubmit = handleSubmit<DeployFormData>(async (data) => {
        const res = await api.deploy({
            name: data.name,
            image: data.image
        });
        toast.dark(
            <Flex direction="row" justify="start" align="center">
                <FontAwesomeIcon icon={faCloudUploadAlt} mr="1.5em" />
                <p>✅ Deployed {res.data.deployment?.id}</p>
            </Flex>
        );
        setLocation("/dashboard/deployments");
    });

    return (
        <form onSubmit={onSubmit}>
            <Flex justify="center" align="center" direction="col" flexWrap>
                <h2 className="mb-2">Create a Deployment</h2>
                <div className="mb-6">
                    <h3 htmlFor="name">Deployment name</h3>
                    <Input
                        className="w-full"
                        placeholder="test"
                        {...register("name", {
                            required: true,
                            minLength: 1
                        })}
                    />
                    <p className="text-red-600 mt-2">
                        {errors.name && errors.name.message}
                    </p>
                </div>
                <div className="mb-6">
                    <h3 htmlFor="image">Deployment image</h3>
                    <Input
                        className="w-full"
                        placeholder="nginx:alpine"
                        {...register("image", {
                            required: true,
                            minLength: 1
                        })}
                    />
                    <p className="text-red-600 mt-2">
                        {errors.image && errors.image.message}
                    </p>
                </div>
                <Button
                    isLoading={isSubmitting}
                    type="submit"
                    className="mb-6 w-full"
                >
                    <FontAwesomeIcon icon={faPaperPlane} />
                    <p className="ml-2">Deploy</p>
                </Button>
            </Flex>
        </form>
    );
};
