import { IDeployment } from "@flowtr/panel-sdk";
import { useEffect, useState } from "preact/hooks";
import { useLocation } from "wouter";
import { api } from "../lib/api";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRedo, faCircle } from "@fortawesome/free-solid-svg-icons";
import { Flex } from "./ui/flex";
import { Button } from "./ui/button";

export const DeploymentInfo = (props: { id: string }) => {
    const [deployment, setDeployment] = useState<IDeployment | undefined>(
        undefined
    );
    const [reload, setReload] = useState<boolean>(false);

    const [, setLocation] = useLocation();

    useEffect(() => {
        api.getDeployment(props.id).then(
            (res) =>
                res.data.containers &&
                res.data.containers.length > 0 &&
                setDeployment(res.data.containers[0])
        );
    }, [reload, props.id]);

    return (
        <Flex justify="center" align="center" direction="col">
            <Button
                onClick={() => setLocation("/dashboard/deployments")}
                className="mb-6"
            >
                Back to Deployment List
            </Button>
            {deployment ? (
                <Flex
                    justify="center"
                    align="center"
                    direction="col"
                    className="bg-gray-700 p-6 border-6 mb-6 mr-6"
                >
                    <Button onClick={() => setReload(!reload)} className="mb-6">
                        <FontAwesomeIcon icon={faRedo} />
                        <p className="ml-2">Reload</p>
                    </Button>
                    <Flex
                        justify="center"
                        align="center"
                        direction="col"
                        className="mb-2"
                    >
                        <h3>Identifier</h3>
                        <p>{deployment.id}</p>
                    </Flex>

                    <Flex
                        justify="center"
                        align="center"
                        direction="col"
                        className="mb-2"
                    >
                        <h3>Status</h3>
                        <Flex justify="center" align="center">
                            <p>
                                {deployment.status ? "Running" : "Not Running"}
                            </p>
                            <FontAwesomeIcon
                                icon={faCircle}
                                className="ml-2"
                                width="1em"
                                bg={deployment.status ? "#42f563" : "#f56642"}
                            />
                        </Flex>
                    </Flex>

                    {deployment.image && (
                        <Flex
                            justify="center"
                            align="center"
                            direction="col"
                            className="mb-2"
                        >
                            <h3>Image</h3>
                            <p>{deployment.image}</p>
                        </Flex>
                    )}

                    {deployment.name && (
                        <Flex
                            justify="center"
                            align="center"
                            direction="col"
                            className="mb-2"
                        >
                            <h3 as="h3">Name</h3>
                            <p>{deployment.name}</p>
                        </Flex>
                    )}
                </Flex>
            ) : (
                <param>Deployment not found.</param>
            )}
        </Flex>
    );
};
