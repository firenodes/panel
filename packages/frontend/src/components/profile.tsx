import { useEffect, useState } from "preact/hooks";
import { IUser, toTitleCase } from "@flowtr/panel-sdk";
import { api } from "../lib/api";
import { useLocation } from "wouter";
import { Flex } from "./ui/flex";

export const Profile = () => {
    const [user, setUser] = useState<IUser | undefined>(undefined);
    const [, setLocation] = useLocation();

    useEffect(() => {
        api.getProfile()
            .then((res) => {
                setUser(res.data.user);
            })
            .catch(() => setLocation("/dashboard/login"));
    }, [setLocation]);

    return (
        <Flex direction="col" justify="center" align="center">
            <h2>Welcome, {toTitleCase(user?.username)}</h2>
        </Flex>
    );
};
