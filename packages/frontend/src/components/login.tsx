import { useForm } from "react-hook-form";
import { api } from "../lib/api";
import { useLocation } from "wouter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignInAlt } from "@fortawesome/free-solid-svg-icons";
import { Flex } from "./ui/flex";
import { Input } from "./ui/input";
import { Button } from "./ui/button";

export interface LoginFormData {
    username: string;
    password: string;
}

export const Login = () => {
    const [, setLocation] = useLocation();
    const {
        register,
        handleSubmit,
        formState: { isSubmitting, errors }
    } = useForm<LoginFormData>();

    const onSubmit = async (data: { username: string; password: string }) => {
        console.log(data);
        await api.login(data);
        setLocation("/dashboard/profile");
    };

    return (
        <Flex direction="col" justify="center" align="center">
            <h2 className="text-bold text-5xl mb-6">Login</h2>
            <div className="mb-6">
                <h3 className="mb-2 text-2xl">Username</h3>
                <Input
                    className="w-full"
                    placeholder="TheoParis"
                    {...register("username", {
                        required: "A username is required",
                        minLength: {
                            value: 3,
                            message:
                                "The username must be at least 3 characters"
                        }
                    })}
                />
                <p className="text-red-600 mt-2">
                    {errors.username && errors.username.message}
                </p>
            </div>
            <div className="mb-6">
                <h3 className="mb-2 text-2xl">Password</h3>
                <Input
                    className="w-full"
                    type="password"
                    placeholder="*****"
                    {...register("password", {
                        required: "A password is required",
                        minLength: {
                            value: 5,
                            message:
                                "The password must be at least 5 characters"
                        }
                    })}
                />
                <p className="text-red-600 mt-2">
                    {errors.password && errors.password.message}
                </p>
            </div>
            <Button
                isLoading={isSubmitting}
                onClick={handleSubmit(onSubmit)}
                className="w-full"
            >
                <Flex direction="row" align="center" justify="center">
                    <FontAwesomeIcon
                        className="mr-4"
                        size="lg"
                        icon={faSignInAlt}
                    />
                    <p>Log In</p>
                </Flex>
            </Button>
            {/* <Button
                    backgroundColor="#5865F2"
                    img
                    onClick={handleSubmit(onSubmit)}
                >
                    <Image
                        boxSize="1.25em"
                        mr="0.50em"
                        src="https://discord.com/assets/9f6f9cd156ce35e2d94c0e62e3eff462.png"
                    />
                    Discord
                </Button> */}
        </Flex>
    );
};
