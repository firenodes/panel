import yargs from "yargs";
import { loadConfig } from "./config.js";
import { connectToDatabase } from "./database/index.js";
import { UserInput, userSchema } from "@flowtr/panel-sdk";
import { argon2id } from "hash-wasm";
import { startBackend } from "./index.js";
import { User } from "./database/entity/user.js";
import crypto from "crypto";
import { logger } from "./logger.js";

const setup = async () => {
    const config = await loadConfig();
    const db = await connectToDatabase(config.database);

    return { config, db };
};

yargs(process.argv.slice(2))
    .scriptName("fdpla")
    .completion()
    .help()
    .alias("h", "help")
    .command(
        "run",
        "Run the backend",
        (yargs) => yargs,
        () =>
            startBackend().catch((err) => {
                logger.error(err);
                process.exit(1);
            })
    )
    .command("user", "User subcommand", (yargs) =>
        yargs
            .command(
                "add",
                "Adds a user to the database",
                (yargs) =>
                    yargs
                        .option("username", {
                            alias: "u",
                            demandOption: true,
                            type: "string",
                        })
                        .option("password", {
                            alias: "p",
                            demandOption: true,
                            type: "string",
                        })
                        .option("super", {
                            alias: "s",
                            type: "boolean",
                            default: false,
                        }),
                async (args) => {
                    try {
                        await setup();
                        const salt = crypto.randomBytes(16);
                        const hashedPassword = await argon2id({
                            password: args.password,
                            salt,
                            parallelism: 1,
                            iterations: 256,
                            memorySize: 512, // use 512KB memory
                            hashLength: 32, // output size = 32 bytes
                            outputType: "encoded",
                        });
                        const raw: UserInput = {
                            username: args.username,
                            password: hashedPassword,
                            roles: [args.super ? "admin" : "guest"],
                        };
                        const parsedUser = await userSchema.parseAsync(raw);
                        User.insert(parsedUser);
                        const result = await User.findOne({
                            id: parsedUser.id,
                        });

                        logger.info(
                            `✅ Created user ${result?.username} with id ${result?.id}`
                        );
                    } catch (err) {
                        logger.error(`❌ ${err}`);
                        process.exit(1);
                    }
                }
            )
            .command(
                "rm",
                "Removes a user from the database",
                (yargs) =>
                    yargs.option("username", {
                        alias: "u",
                        demandOption: true,
                        type: "string",
                    }),
                async (args) => {
                    try {
                        await setup();
                        User.delete({
                            username: args.username,
                        });
                        logger.info(`✅ Deleted user ${args.username}`);
                    } catch (err) {
                        logger.error(`❌ ${err}`);
                        process.exit(1);
                    }
                }
            )
    )
    .parse();
