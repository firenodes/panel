import { connectToDatabase } from "./database/index.js";
import { loadConfig } from "./config.js";
import cors from "fastify-cors";
import userPlugin from "./routes/user.js";
import fastify from "fastify";
import jwt from "fastify-jwt";
import fs from "fs";
import os from "os";
import { logger } from "./logger.js";

export const startBackend = async () => {
    const config = await loadConfig();
    await connectToDatabase(config.database);

    const dataPath = `${os.homedir()}/.fdpl`;
    if (!fs.existsSync(dataPath)) fs.mkdirSync(dataPath);

    const server = fastify({
        logger: process.env.NODE_ENV !== "production",
    });

    server.register(cors, {});
    server.register(jwt, {
        secret: config.jwtSecret,
    });

    // Routes
    server.register(userPlugin);

    try {
        await server.listen(config.port);
        logger.log(`Server listening at :${config.port}`);
    } catch (err) {
        logger.error(err);
        process.exit(1);
    }
};
