import * as z from "zod";
// import { homedir } from "os";
import { OmitIndex } from "@flowtr/panel-sdk";

const configSchema = z.object({
    database: z.object({
        url: z.string().url()
    }),
    deploymentNamespace: z.string().min(1).default("default"),
    jwtExpiresIn: z.string().default("2d"),
    jwtSecret: z.string().min(5).default("12345"),
    sessionKey: z.string().min(5).default("12345"),
    port: z.number().default(8000)
});

export type Config = OmitIndex<z.infer<typeof configSchema>, string>;

export const loadConfig = async (
    path = `${process.cwd()}/${process.env.CONFIG_FILE || "fdpl.mjs"}`
): Promise<Config> => {
    const raw = (await import(path)).default;
    return configSchema.parseAsync(raw);
};
