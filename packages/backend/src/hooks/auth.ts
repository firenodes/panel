import { FastifyRequest, FastifyReply } from "fastify";
import { errors } from "@flowtr/panel-sdk";

export const authenticate = async (req: FastifyRequest, res: FastifyReply) => {
    try {
        await req.jwtVerify();
    } catch (err) {
        return res.code(401).send({
            error: errors.noAuthorizationHeader
        });
    }
};
