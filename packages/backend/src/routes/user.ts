import { argon2id, argon2Verify } from "hash-wasm";
import { errors, IUser, userSchema } from "@flowtr/panel-sdk";
import { FastifyPluginAsync } from "fastify";
import fp from "fastify-plugin";
import { User } from "../database/entity/user.js";
import { loadConfig } from "../config.js";
import { authenticate } from "../hooks/auth.js";

const userPlugin: FastifyPluginAsync = async (fastify) => {
    const config = await loadConfig();

    fastify.post("/auth/login", async (req, res) => {
        const { username, password } = await userSchema.parseAsync(req.body);
        const user = await User.findOne({ username });
        if (!user) {
            return res.code(404).send({
                error: errors.userNotFound
            });
        }
        const hash = await argon2Verify({ password, hash: user.password });
        if (!hash) {
            return res.code(401).send({
                error: errors.invalidPassword
            });
        }
        const serializedUser: Partial<IUser> = { ...user };
        delete serializedUser.password;
        const token = fastify.jwt.sign(
            { user: serializedUser },
            {
                expiresIn: config.jwtExpiresIn
            }
        );

        return res.code(200).send({
            token
        });
    });

    fastify.get(
        "/auth/profile",
        {
            preValidation: [authenticate]
        },
        async (req, res) => {
            const tokenUser = req.user as unknown as IUser;
            const user = await User.findOne({ username: tokenUser.username });
            if (!user) {
                return res.code(404).send({
                    error: errors.userNotFound
                });
            }
            return res.code(200).send({
                user
            });
        }
    );
};

export default fp(userPlugin);
