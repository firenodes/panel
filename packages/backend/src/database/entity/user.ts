import { Entity, PrimaryColumn, Column, BaseEntity } from "typeorm";
import { IUser } from "@flowtr/panel-sdk";

@Entity()
export class User extends BaseEntity implements IUser {
    @PrimaryColumn({ type: "text" })
    id!: IUser["id"];

    @Column({ type: "text" })
    username!: IUser["username"];

    @Column({ type: "text" })
    password!: IUser["password"];

    @Column({ type: "json" })
    roles!: IUser["roles"];
}
