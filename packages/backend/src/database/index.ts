import { Config } from "../config.js";
import { createConnection } from "typeorm";
import { User } from "./entity/user.js";

export const connectToDatabase = async (database: Config["database"]) =>
    createConnection({
        type: "postgres",
        entities: [User],
        synchronize: true,
        url: database.url
    });
