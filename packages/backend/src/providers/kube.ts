import { IDeployment } from "@flowtr/panel-sdk";
import * as k8s from "@kubernetes/client-node";
import { Config } from "../config";

export class KubernetesProvider {
    appsApi: k8s.AppsV1Api;

    constructor(
        public readonly user: string,
        public readonly config: Config,
        public readonly apps: IDeployment[]
    ) {
        const kc = new k8s.KubeConfig();
        kc.loadFromDefault();
        this.appsApi = kc.makeApiClient(k8s.AppsV1Api);
    }

    async appExists(namespace: string, appName: string) {
        try {
            const deployment = await this.appsApi.readNamespacedDeployment(
                appName,
                namespace
            );
            return deployment !== null;
        } catch {
            return false;
        }
    }

    async deploy() {
        for await (const app of this.apps) {
            console.info(`Deploying ${JSON.stringify(app)}`);

            const deploymentData: k8s.V1Deployment = {
                metadata: {
                    labels: {
                        "com.firenodes.panel.deployment.id": app.id,
                        "com.firenodes.panel.deployment.name": app.name,
                        "com.firenodes.panel.deployment.user": this.user
                    },
                    name: app.name
                },
                spec: {
                    selector: {
                        matchLabels: {
                            "com.firenodes.panel.deployment.id": app.id,
                            "com.firenodes.panel.deployment.name": app.name,
                            "com.firenodes.panel.deployment.user": this.user
                        }
                    },
                    template: {
                        metadata: {
                            labels: {
                                "com.firenodes.panel.deployment.id": app.id,
                                "com.firenodes.panel.deployment.name": app.name,
                                "com.firenodes.panel.deployment.user": this.user
                            }
                        },
                        spec: {
                            volumes: [
                                {
                                    name: "data",
                                    hostPath: {
                                        path: `/hosthome/.fdpl/${app.id}/${app.dataPath}`,
                                        type: "DirectoryOrCreate"
                                    }
                                }
                            ],
                            containers: [
                                {
                                    name: app.name,
                                    image: app.image,
                                    ports: app.ports.map((e) => ({
                                        containerPort: e.privatePort,
                                        hostPort: e.publicPort
                                    }))
                                    // env: app.env
                                }
                            ]
                        }
                    }
                }
            };

            if (
                !(await this.appExists(
                    this.config.deploymentNamespace,
                    app.name
                ))
            )
                return (
                    await this.appsApi.createNamespacedDeployment(
                        this.config.deploymentNamespace,
                        deploymentData
                    )
                ).body;
            else
                return (
                    await this.appsApi.replaceNamespacedDeployment(
                        app.name,
                        this.config.deploymentNamespace,
                        deploymentData
                    )
                ).body;
        }
    }
}
