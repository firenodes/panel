import { PrefixLogger, ColorLogger } from "@toes/logger";

export const logger = new PrefixLogger({
    prefix: "Panel Backend",
    separator: " > ",
}).extend(ColorLogger, {
    color: "black",
});
