type EqualsTest<T> = <A>() => A extends T ? 1 : 0;
export type Equals<A1, A2> = EqualsTest<A2> extends EqualsTest<A1> ? 1 : 0;

export type Filter<K, I> = Equals<K, I> extends 1 ? never : K;

export type OmitIndex<T, I extends string | number> = {
    [K in keyof T as Filter<K, I>]: T[K];
};
