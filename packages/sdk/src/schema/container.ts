import { nanoid } from "nanoid";
import { z } from "zod";
import { OmitIndex } from "../types";

export const portSchema = z.object({
    privatePort: z.number(),
    publicPort: z.number()
});
export const deploymentSchema = z.object({
    id: z
        .string()
        .nonempty()
        .default(() => nanoid(32)),
    image: z.string().nonempty(),
    name: z.string().nonempty(),
    labels: z.record(z.string().nonempty()).default({}),
    // TODO: make a container schema and make deployments part of database
    status: z.boolean().default(false),
    dataPath: z.string().default("/data"),
    ports: z.array(portSchema).default([])
});

export type IDeployment = OmitIndex<z.infer<typeof deploymentSchema>, string>;
export type IPort = OmitIndex<z.infer<typeof portSchema>, string>;
export type IDeploymentInput = OmitIndex<
    z.input<typeof deploymentSchema>,
    string
>;
